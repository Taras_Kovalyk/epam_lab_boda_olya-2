using System;

namespace test
{
	class Matrix
	{
		private double[,] _matrix;
		
		public Matrix(int rowCount, int colCount)
		{
			RowCount = rowCount;
			ColCount = colCount;
			_matrix = new double[rowCount, colCount];
		}
		
		public int RowCount { get; set; }
		public int ColCount { get; set; }
		
		public double this[int row, int col]
		{
			get
			{
				if (row < 0 || col < 0 || RowCount <= row || ColCount <= col)
				{
					throw new Exception("Error: Matrix doesn't have element with this index");
				}
				
				return _matrix[row, col];
			}
			set
			{
				if (row < 0 || col < 0 || RowCount <= row || ColCount <= col)
				{
					
					throw new Exception("Error: Matrix doesn't have element with this index");
				}
				
				_matrix[row, col] = value;
			}
		}
		
		public override bool Equals(object obj)
		{
			Matrix matrix = obj as Matrix;
			
			if ((matrix == null) || (matrix.RowCount != this.RowCount) || (matrix.ColCount != this.ColCount))
			{
				return false;
			} 
			
			for (var i = 0; i < matrix.RowCount; i++)
			{
				for (var j = 0; j < matrix.ColCount; j++)
				{
					if (matrix[i, j] != this[i, j])
					{
						return false;
					}
				}  
			}
			
		 return true;	
		}
		
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		
		public static bool operator ==(Matrix matrix1, Matrix matrix2)
		{
			return Equals(matrix1, matrix2);
		}
		
		public static bool operator !=(Matrix matrix1, Matrix matrix2)
		{
			return !(Equals(matrix1, matrix2));
		}
		
		public static bool operator >(Matrix matrix1, Matrix matrix2)
		{
			if ((matrix1.RowCount != matrix2.RowCount) || (matrix1.ColCount != matrix2.ColCount))
			{
				return false;
			}
			for (var i = 0; i < matrix1.RowCount; i++)
			{
				for (var j = 0; j < matrix1.ColCount; j++)
				{
					if (matrix1[i, j] != matrix2[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		
		public static bool operator <(Matrix matrix1, Matrix matrix2)
		{
			if ((matrix1.RowCount != matrix2.RowCount) || (matrix1.ColCount != matrix2.ColCount))
			{
				return false;
			}
			for (var i = 0; i < matrix1.RowCount; i++)
			{
				for (var j = 0; j < matrix1.ColCount; j++)
				{
					if (matrix1[i, j] > matrix2[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		public static bool operator <=(Matrix matrix1, Matrix matrix2)
		{
			if ((matrix1.RowCount != matrix2.RowCount) || (matrix1.ColCount != matrix2.ColCount))
			{
				return false;
			}
			for (var i = 0; i < matrix1.RowCount; i++)
			{
				for (var j = 0; j < matrix1.ColCount; j++)
				{
					if (matrix1[i, j] > matrix2[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		
		public static bool operator >=(Matrix matrix1, Matrix matrix2)
		{
			if ((matrix1.RowCount != matrix2.RowCount) || (matrix1.ColCount != matrix2.ColCount))
			{
				return false;
			}
			for (var i = 0; i < matrix1.RowCount; i++)
			{
				for (var j = 0; j < matrix1.ColCount; j++)
				{
					if (matrix1[i, j] < matrix2[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		
		public static Matrix operator +(Matrix matrix1, Matrix matrix2)
		{
			if (matrix1.RowCount != matrix2.RowCount || matrix1.ColCount != matrix2.ColCount)
			{
				throw new Exception("Error: different size of matrix.");
			}
			
			var result = new Matrix(matrix1.RowCount, matrix1.ColCount);
			
			for (var i = 0; i < result.RowCount; i++)
			{
				for (var j = 0; j < result.ColCount; j++)
				{
					result[i, j] = matrix1[i, j] + matrix2[i, j];
				}
			}
			
			return result;
		}
		
		public static Matrix operator - (Matrix matrix1, Matrix matrix2)
		{
			
			if (matrix1.RowCount != matrix2.RowCount || matrix1.ColCount != matrix2.ColCount)
			{
				throw new Exception("Error: different size of matrix.");
			}
			
			var result = new Matrix(matrix1.RowCount, matrix1.ColCount);
			
			for (var i = 0; i < result.RowCount; i++)
			{
				for (var j = 0; j < result.ColCount; j++)
				{
					result[i, j] = matrix1[i, j] - matrix2[i, j];
				}
			}
			
			return result;
		}
		
		public static Matrix operator *(Matrix matrix1, Matrix matrix2)
		{
			
			if (matrix1.ColCount != matrix2.RowCount)
			{
				throw new Exception("Error: can't multiply this matrix.");
			}
			
			var result = new Matrix(matrix1.RowCount, matrix2.ColCount);
			
			for (var i = 0; i < result.RowCount; i++)
			{
				for (var j = 0; j < result.ColCount; j++)
				{
					for (var k = 0; k < matrix1.ColCount; k++)
					{
						result[i, j] += matrix1[i, k] * matrix2[k, j];
					}
				}
			}
			
			return result;
		}
		public Matrix GetTransposeMatrix()
		{
			var result = new Matrix(this.ColCount, this.RowCount);
			
			for (var i = 0; i < this.ColCount; i++)
			{
				for (var j = 0; j < this.RowCount; j++)
				{
					result[i, j] = this[j, i];				
				}
			}
			
			return result;
		}
		
		public Matrix MultiplyAtNumber(double number)
		{
			var result = new Matrix(this.RowCount, this.ColCount);
			
			for (var i = 0; i < this.RowCount; i++)
			{
				for (var j = 0; j < this.ColCount; j++)
				{
					result[i, j] = this[i, j] * number;
				}
			}
			
			return result;
		}
		
		public Matrix GetSubMatrix(int beginRow, int beginCol, int row, int col)
		{
			if (beginRow < 0 || beginCol < 0)
			{
				throw new Exception("Error: Matrix doesn't have element with this index");
			}
			
			if (this.RowCount - beginRow < row || this.ColCount - beginCol < col)
			{
				throw new Exception("Error: can't get matrix of such size");	
			}
			
			var result = new Matrix(row, col);
			var difRow = 0 - beginRow;
			var difCol = 0 - beginCol;
			
			for (var i = beginRow; i < row + beginRow; i++)
			{
				for (var j = beginCol; j < col + beginCol; j++)
				{
					result[(i + difRow),(j + difCol)] = this[i, j];
				}
			}
			return result;
		}
		

		public void Print()
		{
			for (var i = 0; i < this.RowCount; i++)
			{
				for (var j = 0; j < this.ColCount; j++)
				{
					Console.Write("{0}\t", this[i, j]);
				}
				
				Console.WriteLine("\n");
			} 
		}
		 
		public void Fill(int rand)
		{
			Random rnd = new Random();

			for (var i = 0; i < this.RowCount; i++)
			{
				for (var j = 0; j < this.ColCount; j++)
				{
					this[i, j] = rnd.Next(i * j + rand);
				}
			}
		} 
		
	}

}
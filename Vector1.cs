using System;
namespace test
{
	public class Vector
	{
		public Vector(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		public double X { get; set; }

		public double Y { get; set; }

		public double Z { get; set; }

		public double Length
		{
			get
			{
				return Math.Sqrt(Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2));
			}
		}

		public double GetScalarProduct(Vector vec)
		{
			if (vec == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			return this.X * vec.X + this.Y * vec.Y + this.Z * vec.Z;
		}

		public Vector GetCrossProduct(Vector vec)
		{
			if (vec == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			return new Vector(this.Y * vec.Z - this.Z * vec.Y, this.Z * vec.X - this.X * vec.Z, this.X * vec.Y - this.Y * vec.X);
		}

		public double GetTripleProduct(Vector vec1, Vector vec2)
		{
			if (vec1 == null || vec2 == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			return this.GetScalarProduct(vec1.GetCrossProduct(vec2));
		}

		public double GetAngle(Vector vec)
		{
			if (vec == null)
			{
				throw new ArgumentNullException("Methog get null argument");
			}
			
			return (Math.Acos(this.GetScalarProduct(vec) / (this.Length * vec.Length))) * 180 / Math.PI;
		}

		public override bool Equals(object obj)
		{
			Vector vec = obj as Vector;

			if (vec == null)
			{
				return false;
			}

			return (this.X == vec.X) && (this.Y == vec.Y) && (this.Z == vec.Z);
		}

		public override int GetHashCode()
		{
			return (int)X ^ (int)Y ^ (int)Z;
		}

		public static bool operator ==(Vector vec1, Vector vec2)
		{
			return Equals(vec1, vec2);
		}

		public static bool operator !=(Vector vec1, Vector vec2)
		{
			return !(Equals(vec1, vec2));
		}

		public static bool operator >(Vector vec1, Vector vec2)
		{
			return vec1.Length > vec2.Length;
		}

		public static bool operator <(Vector vec1, Vector vec2)
		{
			return vec1.Length < vec2.Length;
		}

		public static bool operator >=(Vector vec1, Vector vec2)
		{
			return vec1.Length >= vec2.Length;
		}

		public static bool operator <=(Vector vec1, Vector vec2)
		{
			return vec1.Length <= vec2.Length;
		}

		public static Vector operator +(Vector vec1, Vector vec2)
		{
			return new Vector(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
		}

		public static Vector operator -(Vector vec1, Vector vec2)
		{
			return new Vector(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
		}

		public override string ToString()
		{
			return string.Format("[{0}, {1}, {2}]", this.X, this.Y, this.Z);
		}
	}

	{
		public static void Main()
		{
			var vector1 = new Vector(3, 4, 5);
			var vector2 = new Vector(3, 4, 5);
			var vector3 = new Vector(5, 7, 8);
			var vector4 = new Vector(1, 7, 8);
			var crossProductVec = vector1.GetCrossProduct(vector3);
			var subtractVec = vector2 - vector3;
			var addVec = vector1 + vector2;

			Console.WriteLine("Input data:");
			Console.WriteLine("\tvector1 = {0}", vector1);
			Console.WriteLine("\tvector2 = {0}", vector2);
			Console.WriteLine("\tvector3 = {0}", vector3);
			Console.WriteLine("\tvector4 = {0}", vector4);

			Console.WriteLine("\nUsage demo:\n");
			Console.WriteLine("The length of each vectors:\n\tvector1 = {0:#.##};\n\tvector2 = {1:#.##};\n\tvector3 = {2:#.##};\n\tvector4 = {3:#.##};\n",
			vector1.Length, vector2.Length, vector3.Length, vector4.Length);

			Console.WriteLine("Scalar product: vector1 * vector3 = {0}\n", vector1.GetScalarProduct(vector3));
			Console.WriteLine("Cross product: vector1 x vector3 = {0}\n", crossProductVec);
			Console.WriteLine("Triple product: vector1 * (vector2 x vector3) = {0:#0.##}\n", vector1.GetTripleProduct(vector2, vector3));
			Console.WriteLine("Angle beatween vector1 and vectot2 is: {0:#0.##}\n", vector1.GetAngle(vector2));

			Console.WriteLine("vector2 - vector3 = {0}\n", subtractVec);
			Console.WriteLine("vector1 + vector2 = {0}\n", addVec);

			Console.WriteLine("vector1 == vector2: {0}\n", vector1 == vector2);
			Console.WriteLine("vector1 != vector3: {0}\n", vector1 != vector3);
			Console.WriteLine("vector1 > vector3: {0}\n", vector1 > vector3);
			Console.WriteLine("vector2 < vector4: {0}", vector2 < vector4);
			
			Console.ReadKey();
		}
	}*/
}